/*
 * #%L
 * lema-provider-log4j
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import org.apache.log4j.Logger;

public final class EventGenerator {

	private static final Logger LOG = Logger.getLogger(EventGenerator.class);

	private EventGenerator() {

	}

	public static void main(String[] args) throws Exception{
		for (int i = 1; i <= 5; i++) {
			MDCEnum.modul.put("modul"+i);
			MDCEnum.user.put("user"+i);
			MDCEnum.subkomp.put("subk"+i);
			MDCEnum.type.put("type"+i);
			MDCEnum.session.put("session"+i);
			MDCEnum.serverId.put("ser"+i);
			MDCEnum.clientId.put("cli"+i);

			LOG.info("Message Nr. " + i);
			System.out.println("Message Nr. " + i);
			//logEx(i);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Thread.sleep(12000);
		for (int i = 6; i <= 7; i++) {
			LOG.info("Message Nr. " + i);
			System.out.println("Message Nr. " + i);
			//logEx(i);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Thread.sleep(12000);
		for (int i = 8; i <= 9; i++) {
			LOG.info("Message Nr. " + i);
			System.out.println("Message Nr. " + i);
			//logEx(i);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		Thread.sleep(300000);
	}

	private static void logEx(int i) {
		LOG.error("Prog1, " + i, new RuntimeException("bla2"));
	}
}

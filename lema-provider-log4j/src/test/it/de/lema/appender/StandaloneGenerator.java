/*
 * #%L
 * lema-provider-log4j
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import org.apache.log4j.Level;


public class StandaloneGenerator {
	public static void main(String[] args) {
		LemaAppender app = new LemaAppender();
		app.setApplication("a");
		app.setEnvironmentId("150");
		app.setRemoteHost("127.0.0.1");
		app.setApplicationVersion("1.2.2");
		app.setSendeBeacon(true);
		app.setHostname("Client1");
		app.setConnectOnDemand(true);
		app.setReconnectionDelay("1000");
		app.activateOptions();
		for (int i = 0; i < 200000; i++) {
			ExtendedLoggingEvent le = LoggingEventFactory.createExtendedLoggingEvent((i % 7 == 0) ? Level.INFO : Level.INFO, "This is an error.",
					new ArrayIndexOutOfBoundsException("ExampleError:" + (i / 30)));
			le.setModul("" + i / 100);
			le.setKlassifizierung("Server");
			le.setSessionId("Session:" + i / 10);

			app.append(le);

			le = LoggingEventFactory.createExtendedLoggingEvent((i % 7 == 0) ? Level.DEBUG : Level.INFO, "Another Error." + i, null);
			le.setModul("" + i / 100);
			le.setUser("f" + i / 100);

			app.append(le);

			le = LoggingEventFactory.createExtendedLoggingEvent((i % 7 == 0) ? Level.WARN : Level.FATAL, "",
					new RuntimeException("Error:" + (i / 30)));
			le.setModul("" + i / 100);
			le.setUser("e" + i / 100);

			app.append(le);
			try {
				Thread.sleep(20000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}

/*
 * #%L
 * lema-provider-log4j
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.util;

import java.util.ArrayList;
import java.util.Enumeration;

import org.apache.log4j.Appender;
import org.apache.log4j.LogManager;

import de.lema.appender.LemaAppender;

public class ShutdownHelper {

	public static void unregisterLemaAppenders() {
		@SuppressWarnings("unchecked")
		Enumeration<Object> allAppenders = LogManager.getRootLogger().getAllAppenders();
		ArrayList<Appender> remove = new ArrayList<Appender>();
		while (allAppenders.hasMoreElements()) {
			Object appender = allAppenders.nextElement();
			if (appender instanceof LemaAppender) {
				LemaAppender socketAppender = (LemaAppender) appender;
				socketAppender.close();
				remove.add(socketAppender);
			}
		}
		for (Appender toRemove : remove) {
			LogManager.getRootLogger().removeAppender(toRemove);
		}
	}
}

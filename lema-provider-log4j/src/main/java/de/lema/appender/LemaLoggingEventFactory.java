/*
 * #%L
 * lema-provider-log4j
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map.Entry;

import org.apache.log4j.MDC;
import org.apache.log4j.spi.LocationInfo;
import org.apache.log4j.spi.LoggingEvent;

public class LemaLoggingEventFactory {

	/**
	 * @param event        LoggingEvent
	 * @param locationInfo
	 * @return LemaLoggingEvent
	 */
	public static LemaLoggingEvent create(final LoggingEvent event, final boolean locationInfo) {

		String message = event.getRenderedMessage();

		Throwable throwable = (event.getThrowableInformation() != null) ?
				event.getThrowableInformation()
				     .getThrowable() :
				null;
		final String exceptionKlasse = (throwable != null) ?
				(throwable instanceof ExceptionHelper ?
						((ExceptionHelper) throwable).getClassName() :
						throwable.getClass()
						         .getName()) :
				null;
		final String exceptionMessage = (throwable != null) ? throwable.getMessage() : null;

		long datum = event.timeStamp;
		int level = event.getLevel()
		                 .toInt();

		LocationInfo locationInformation = null;
		if (locationInfo) {
			locationInformation = event.getLocationInformation();

		}
		String className = getKlasse(event, locationInformation);
		String methodName = (locationInformation != null) ? locationInformation.getMethodName() : null;
		int lineNumber = 0;
		if (locationInformation != null && locationInformation.getLineNumber() != null && !locationInformation.getLineNumber()
		                                                                                                      .equals(LocationInfo.NA)) {
			try {
				lineNumber = Integer.parseInt(locationInformation.getLineNumber());
			} catch (NumberFormatException e) {

			}
		}

		String threadName = event.getThreadName();
		HashMap<String, String> mdc = new HashMap<>();

		@SuppressWarnings("unchecked") Hashtable<String, Object> context = MDC.getContext();
		if (context != null && !context.isEmpty()) {
			mdc = new HashMap<>();
			for (Entry<String, Object> entry : context.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				if (key != null && value != null) {
					mdc.put(key, value.toString());
				}
			}
		}

		return new LemaLoggingEvent(message, mdc, throwable, null, datum, level, lineNumber, className, methodName, threadName, exceptionKlasse,
		                            exceptionMessage);

	}

	private static String getKlasse(LoggingEvent event, LocationInfo locationInfo) {
		if (locationInfo == null || LocationInfo.NA.equals(locationInfo.getClassName())) {
			return event.getLoggerName();
		} else {
			return locationInfo.getClassName();
		}

	}
}

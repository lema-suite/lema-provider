/*
 * #%L
 * lema-provider-log4j2
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.logging.log4j.core.AbstractLifeCycle;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Core;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.appender.AbstractAppender;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginElement;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import de.lema.annotations.ThreadSafe;
import de.lema.appender.net.ConnectionProperties;
import de.lema.appender.net.SocketThread;
import de.lema.appender.net.SocketThreadFactory;

/**
 * Asynchroner Socket-Appender. Die Klasse verwaltet die Attribute und stellt
 * Log4j2 zufrieden; das eigentliche Senden uebernimmt der EventSender.
 */
@ThreadSafe
@Plugin(name = "LemaAppender", category = Core.CATEGORY_NAME, elementType = Appender.ELEMENT_TYPE, printObject = true)
public final class LemaAppender extends AbstractAppender {

	@Override
	public boolean requiresLocation() {
		return true;
	}

	private String application = LemaAppenderDefaults.DEFAULT_APPLICATION;

	private final AtomicReference<SocketThread> socketThread;

	private String port = LemaAppenderDefaults.DEFAULT_PORT;

	private String reconnectionDelay = LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY;

	private String host = LemaAppenderDefaults.DEFAULT_REMOTE_HOST;

	private String environment = LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID;

	private String applicationVersion = "";

	private String hostname = "";

	private int bufferSize = LemaAppenderDefaults.BUFFER_SIZE_DEFAULT;

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	private int connectOnDemandDisconnectTime = 60000;
	private boolean connectOnDemand = false;

	public void setConnectOnDemand(boolean connectOnDemand) {
		this.connectOnDemand = connectOnDemand;
	}

	public int getConnectOnDemandDisconnectTime() {
		return connectOnDemandDisconnectTime;
	}

	public void setConnectOnDemandDisconnectTime(int connectOnDemandDisconnectTime) {
		this.connectOnDemandDisconnectTime = connectOnDemandDisconnectTime;
	}

	public boolean isConnectOnDemand() {
		return connectOnDemand;
	}

	private String connectionLostStrategy = "";
	private String connectionLostStrategyParameter = "";

	public String getConnectionLostStrategyParameter() {
		return connectionLostStrategyParameter;
	}

	public void setConnectionLostStrategyParameter(String connectionLostStrategyParameter) {
		this.connectionLostStrategyParameter = connectionLostStrategyParameter;
	}

	public String getConnectionLostStrategy() {
		return connectionLostStrategy;
	}

	public void setConnectionLostStrategy(String connectionLostStrategy) {
		this.connectionLostStrategy = connectionLostStrategy;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public boolean getSendeBeacon() {
		return sendeBeacon;
	}

	public void setSendeBeacon(boolean sendeBeacon) {
		this.sendeBeacon = sendeBeacon;
	}

	private boolean sendeBeacon = true;

	private final SocketThreadFactory socketThreadFactory;

	protected LemaAppender(String name, Filter filter) {
		super(name, filter, null);
		socketThread = new AtomicReference<>();
		this.socketThreadFactory = SocketThreadFactory.INSTANCE;
	}

	@PluginFactory
	public static LemaAppender createAppender(@PluginAttribute("name") String name,
			@PluginElement("Filter") Filter filter,
			@PluginAttribute("lemaHost") String lemaHost,
			@PluginAttribute("lemaPort") String lemaPort,
			@PluginAttribute("myHostname") String myHostname,
			@PluginAttribute("application") String application,
			@PluginAttribute("environment") String environment,
			@PluginAttribute("applicationVersion") String applicationVersion,
			@PluginAttribute("connectOnDemand") Boolean connectOnDemand) {
		LemaAppender lemaAppender = new LemaAppender(name, filter);
		lemaAppender.setHost(lemaHost);
		lemaAppender.setApplication(application);
		lemaAppender.setEnvironment(environment);
		if (applicationVersion != null) {
			lemaAppender.setApplicationVersion(applicationVersion);
		}
		if (lemaPort != null) {
			lemaAppender.setPort(lemaPort);
		}
		if (connectOnDemand != null) {
			lemaAppender.setConnectOnDemand(connectOnDemand);
		}
		if (myHostname != null) {
			lemaAppender.setHostname(myHostname);
		}
		return lemaAppender;
	}

	public String resolve(String variable, String def) {
		if (variable != null) {
			String resolve = variable.trim();
			if (resolve.length() > 3 && resolve.startsWith("${") && resolve.endsWith("}")) {
				return System.getProperty(resolve.substring(2, resolve.length() - 1), def);
			}
		}
		return variable;
	}

	/**
	 * Startet den Sender; Scheitert dies hier wird es nicht nocheinmal
	 * probiert, da ein Fataler Fehler vorliegt.
	 * LIFECLYCE
	 */
	@Override
	public synchronized void start() {

		// die Umgebungsvariablen aufloesen
		setApplication(resolve(getApplication(), LemaAppenderDefaults.DEFAULT_APPLICATION));
		setEnvironment(resolve(getEnvironment(), LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID));
		setHost(resolve(getHost(), LemaAppenderDefaults.DEFAULT_REMOTE_HOST));
		setReconnectionDelay(resolve(getReconnectionDelay(), LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY));
		setPort(resolve(getPort(), LemaAppenderDefaults.DEFAULT_PORT));
		setHostname(resolve(getHostname(), ""));
		try {
			stopSender();
			final Identifikation ident = Identifikation.create(getApplication(), getEnvironmentIdAsInt(), !isConnectOnDemand() && getSendeBeacon(),
			                                                   getApplicationVersion(), getHostname(), true);
			final ConnectionProperties connectionProperties = new ConnectionProperties(ident, getHost(), getPortAsInt());

			// Nur wenn die Adresse bekannt ist, geht es weiter
			if (ident.istValid() && connectionProperties.istValid()) {
				final SocketThread update = createSenderController(ident, connectionProperties);
				socketThread.set(update);
			} else {
				AbstractLifeCycle.LOGGER.error("Applikation nicht gesetzt oder Addresse nicht korrekt");
			}

		} catch (Exception e) {
			AbstractLifeCycle.LOGGER.error("Fataler Fehler beim Initialisieren des Senders", e);
		}
		super.start();
	}

	public SocketThread createSenderController(final Identifikation ident, final ConnectionProperties connectionProperties) {
		return socketThreadFactory.createInstance(getName(), connectionProperties, getReconnectionDelayAsInt(), ident, connectOnDemand,
		                                          connectOnDemandDisconnectTime, getBufferSize() > 0 ? getBufferSize() : LemaAppenderDefaults.BUFFER_SIZE_DEFAULT);
	}

	private long getReconnectionDelayAsInt() {
		try {
			return Integer.parseInt(reconnectionDelay);
		} catch (NumberFormatException e) {
			AbstractLifeCycle.LOGGER.error("ReconnectionDelay nicht gesetzt bzw. keine Zahl: '" + reconnectionDelay + "'. Setzte Default.");
			return Integer.parseInt(LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY);
		}
	}

	private int getPortAsInt() {
		try {
			return Integer.parseInt(port);
		} catch (NumberFormatException e) {
			AbstractLifeCycle.LOGGER.error("Port nicht gesetzt bzw. keine Zahl: '" + port + "'. Setzte Default.");
			return Integer.parseInt(LemaAppenderDefaults.DEFAULT_PORT);
		}
	}

	@Override
	public synchronized boolean stop(final long timeout, final TimeUnit timeUnit) {
		setStopping();
		super.stop(timeout, timeUnit, false);
		stopSender();
		setStopped();
		return true;
	}

	private synchronized void stopSender() {
		SocketThread old = socketThread.getAndSet(null);
		if (old != null) {
			old.cancel();
		}
	}

	public String getApplication() {
		return application;
	}

	public String getPort() {
		return port;
	}

	public String getReconnectionDelay() {
		return reconnectionDelay;
	}

	public String getHost() {
		return host;
	}

	public String getEnvironment() {
		return environment;
	}

	public int getEnvironmentIdAsInt() {
		try {
			return Integer.parseInt(environment);
		} catch (NumberFormatException e) {
			AbstractLifeCycle.LOGGER.error("EnvironmentId nicht gesetzt bzw. keine Zahl: '" + environment + "'. Setzte Default.");
			return Integer.parseInt(LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID);
		}
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public void setReconnectionDelay(String delay) {
		reconnectionDelay = delay;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@Override
	public void append(final LogEvent logEvent) {
		if (logEvent != null && !isStopped()) {
			try {
				LemaLoggingEvent e = LemaLoggingEventFactory.create(logEvent);
				final SocketThread instance = socketThread.get();
				if (instance != null) {
					instance.enqueForSending(e);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}
	}

}

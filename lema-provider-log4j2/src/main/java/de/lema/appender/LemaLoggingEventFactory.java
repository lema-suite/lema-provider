/*
 * #%L
 * lema-provider-log4j2
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.spi.StandardLevel;
import org.apache.logging.log4j.util.ReadOnlyStringMap;

public class LemaLoggingEventFactory {

	/**
	 * @param event LoggingEvent
	 * @return LemaLoggingEvent
	 */
	public static LemaLoggingEvent create(final LogEvent event) {

		Message message = event.getMessage();

		Throwable throwable = (event.getThrown() != null) ? event.getThrown() : null;
		final String exceptionKlasse = (throwable != null) ?
				(throwable instanceof ExceptionHelper ?
						((ExceptionHelper) throwable).getClassName() :
						throwable.getClass()
						         .getName()) :
				null;
		final String exceptionMessage = (throwable != null) ? throwable.getMessage() : null;

		long datum = event.getTimeMillis();
		int level = getLevel(event);

		StackTraceElement locationInformation = event.getSource();
		String className = getKlasse(event, locationInformation);
		String methodName = (locationInformation != null) ? locationInformation.getMethodName() : null;
		int lineNumber = 0;
		if (locationInformation != null && locationInformation.getLineNumber() > 0) {
			lineNumber = locationInformation.getLineNumber();
		}

		String threadName = event.getThreadName();
		HashMap<String, String> mdc = new HashMap<>();
		ReadOnlyStringMap context = event.getContextData();
		if (context != null && !context.isEmpty()) {
			for (Entry<String, String> entry : context.toMap()
			                                          .entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				if (key != null && value != null) {
					mdc.put(key, value);
				}
			}
		}

		return create(message.getFormattedMessage(), throwable, exceptionKlasse, exceptionMessage, datum, level, className, methodName, lineNumber, threadName,
		              mdc);

	}

	private static int getLevel(final LogEvent event) {
		StandardLevel level = event.getLevel()
		                           .getStandardLevel();
		switch (level) {
			case FATAL:
				return 50000;
			case ERROR:
				return 40000;
			case WARN:
				return 30000;
			case INFO:
				return 20000;
			case DEBUG:
				return 10000;
			case TRACE:
				return 500;
			case ALL:
				return 0;
			default:
				return Integer.MAX_VALUE;
		}

	}


	private static LemaLoggingEvent create(final String message, final Throwable throwable, final String exceptionKlasse, final String exceptionMessage,
			final long datum, final int level, final String className, final String methodName, final int lineNumber, final String threadName,
			final HashMap<String, String> mdc) {

		return new LemaLoggingEvent(message, mdc, throwable, null, datum, level, lineNumber, className, methodName, threadName, exceptionKlasse,
		                            exceptionMessage);
	}

	private static String getKlasse(LogEvent event, StackTraceElement locationInfo) {
		if (locationInfo == null) {
			return event.getLoggerName();
		} else {
			return locationInfo.getClassName();
		}

	}
}

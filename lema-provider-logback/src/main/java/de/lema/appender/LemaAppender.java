/*
 * #%L
 * lema-provider-logback
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.util.concurrent.atomic.AtomicReference;

import ch.qos.logback.classic.net.SocketNode;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.AppenderBase;
import de.lema.annotations.ThreadSafe;
import de.lema.appender.net.ConnectionProperties;
import de.lema.appender.net.SocketThread;
import de.lema.appender.net.SocketThreadFactory;

/**
 * Asynchroner Socket-Appender. Die Klasse verwaltet die Attribute und stellt
 * Logback zufrieden; das eigentliche Senden uebernimmt der EventSender.
 * 
 */
@ThreadSafe
public final class LemaAppender extends AppenderBase<LoggingEvent> {

	private String application = LemaAppenderDefaults.DEFAULT_APPLICATION;

	private final AtomicReference<SocketThread> socketThread;

	private boolean locationInfo = true;

	private String port = LemaAppenderDefaults.DEFAULT_PORT;

	private String reconnectionDelay = LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY;

	private String remoteHost = LemaAppenderDefaults.DEFAULT_REMOTE_HOST;

	private String environmentId = LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID;

	private String applicationVersion = "";

	private String hostname = "";

	private boolean appendExtraInfo = true;

	private int bufferSize = LemaAppenderDefaults.BUFFER_SIZE_DEFAULT;

	public int getBufferSize() {
		return bufferSize;
	}

	public void setBufferSize(int bufferSize) {
		this.bufferSize = bufferSize;
	}

	private int connectOnDemandDisconnectTime = 60000;
	private boolean connectOnDemand = false;

	public void setConnectOnDemand(boolean connectOnDemand) {
		this.connectOnDemand = connectOnDemand;
	}

	public int getConnectOnDemandDisconnectTime() {
		return connectOnDemandDisconnectTime;
	}

	public void setConnectOnDemandDisconnectTime(int connectOnDemandDisconnectTime) {
		this.connectOnDemandDisconnectTime = connectOnDemandDisconnectTime;
	}

	public boolean isConnectOnDemand() {
		return connectOnDemand;
	}

	public boolean isAppendExtraInfo() {
		return appendExtraInfo;
	}

	public void setAppendExtraInfo(boolean appendExtraInfo) {
		this.appendExtraInfo = appendExtraInfo;
	}

	private String connectionLostStrategy = "";
	private String connectionLostStrategyParameter = "";

	public String getConnectionLostStrategyParameter() {
		return connectionLostStrategyParameter;
	}

	public void setConnectionLostStrategyParameter(String connectionLostStrategyParameter) {
		this.connectionLostStrategyParameter = connectionLostStrategyParameter;
	}

	public String getConnectionLostStrategy() {
		return connectionLostStrategy;
	}

	public void setConnectionLostStrategy(String connectionLostStrategy) {
		this.connectionLostStrategy = connectionLostStrategy;
	}

	public String getApplicationVersion() {
		return applicationVersion;
	}

	public void setApplicationVersion(String applicationVersion) {
		this.applicationVersion = applicationVersion;
	}

	public boolean getSendeBeacon() {
		return sendeBeacon;
	}

	public void setSendeBeacon(boolean sendeBeacon) {
		this.sendeBeacon = sendeBeacon;
	}

	private boolean sendeBeacon = true;

	private final SocketThreadFactory socketThreadFactory;

	LemaAppender(SocketThreadFactory socketThreadFactory) {
		socketThread = new AtomicReference<>();
		this.socketThreadFactory = socketThreadFactory;
	}

	public LemaAppender() {
		this(SocketThreadFactory.INSTANCE);
	}

	public String resolve(String variable, String def) {
		if (variable != null) {
			String resolve = variable.trim();
			if (resolve.length() > 3 && resolve.startsWith("${") && resolve.endsWith("}")) {
				return System.getProperty(resolve.substring(2, resolve.length() - 1), def);
			}
		}
		return variable;
	}

	/**
	 * Startet den Sender; Scheitert dies hier wird es nicht nocheinmal
	 * probiert, da ein Fataler Fehler vorliegt.
	 */
	@Override
	public synchronized void start() {

		// die Umgebungsvariablen aufloesen
		setApplication(resolve(getApplication(), LemaAppenderDefaults.DEFAULT_APPLICATION));
		setEnvironmentId(resolve(getEnvironmentId(), LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID));
		setRemoteHost(resolve(getRemoteHost(), LemaAppenderDefaults.DEFAULT_REMOTE_HOST));
		setReconnectionDelay(resolve(getReconnectionDelay(), LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY));
		setPort(resolve(getPort(), LemaAppenderDefaults.DEFAULT_PORT));
		setHostname(resolve(getHostname(), ""));

		try {
			stopSender();

			final Identifikation ident = Identifikation.create(getApplication(), getEnvironmentIdAsInt(), !isConnectOnDemand() && getSendeBeacon(),
					getApplicationVersion(), getHostname(), isAppendExtraInfo());
			final ConnectionProperties connectionProperties = new ConnectionProperties(ident, getRemoteHost(), getPortAsInt());

			if(!ident.istValid()){
				addWarn("Application identification is not valid");
			}

			if(!connectionProperties.istValid()){
				addWarn("Could not resolve Hostname or ip address '" + getRemoteHost() + "'" );
			}

			if (ident.istValid() && connectionProperties.istValid()) {
				final SocketThread update = createSenderController(ident, connectionProperties);
				socketThread.set(update);
			}

		} catch (Exception e) {
			addWarn("Unexpected exception during sender startup", e);
		}
		super.start();
	}

	public SocketThread createSenderController(final Identifikation ident, final ConnectionProperties connectionProperties) {
		return socketThreadFactory.createInstance(getName(), connectionProperties, getReconnectionDelayAsInt(), ident, connectOnDemand,
				connectOnDemandDisconnectTime, getBufferSize() > 0 ? getBufferSize() : LemaAppenderDefaults.BUFFER_SIZE_DEFAULT);
	}

	private long getReconnectionDelayAsInt() {
		try {
			return Integer.parseInt(reconnectionDelay);
		} catch (NumberFormatException e) {
			addWarn("Configured reconnectionDelay is not valid '" + reconnectionDelay +
					"', value is set to default (" + LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY + " )");
			return Integer.parseInt(LemaAppenderDefaults.DEFAULT_RECONNECTION_DELAY);
		}
	}

	private int getPortAsInt() {
		try {
			return Integer.parseInt(port);
		} catch (NumberFormatException e) {
			addWarn("Configured port is not valid '" +  port + "', value is set to default (" + LemaAppenderDefaults.DEFAULT_PORT + " )");
			return Integer.parseInt(LemaAppenderDefaults.DEFAULT_PORT);
		}
	}

	@Override
	public void append(LoggingEvent event) {
		if (event != null && isStarted()) {
			try {
				LemaLoggingEvent e = LemaLoggingEventFactory.create(event, getLocationInfo());
				final SocketThread instance = socketThread.get();
				if (instance != null) {
					instance.enqueForSending(e);
				}
			} catch (Exception e) {
				// Ignore
			}
		}
	}

	@Override
	public synchronized void stop() {
		if (isStarted()) {
			super.stop();
			stopSender();
		}
	}

	private synchronized void stopSender() {
		SocketThread old = socketThread.getAndSet(null);
		if (old != null) {
			old.cancel();
		}
	}

	public String getApplication() {
		return application;
	}

	public boolean getLocationInfo() {
		return locationInfo;
	}

	public String getPort() {
		return port;
	}

	public String getReconnectionDelay() {
		return reconnectionDelay;
	}

	public String getRemoteHost() {
		return remoteHost;
	}

	public String getEnvironmentId() {
		return environmentId;
	}

	public int getEnvironmentIdAsInt() {
		try {
			return Integer.parseInt(environmentId);
		} catch (NumberFormatException e) {
			addWarn("Configured EnvironmentId is not valid '" +  environmentId +
					"', value is set to default (" + LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID + " )");
			return Integer.parseInt(LemaAppenderDefaults.DEFAULT_ENVIRONMENT_ID);
		}
	}

	/**
	 * The SocketAppender does not use a layout. Hence, this method returns
	 * <code>false</code>.
	 * */
	public boolean requiresLayout() {
		return false;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	/**
	 * The <b>LocationInfo</b> option takes a boolean value. If true, the
	 * information sent to the remote host will include location information.
	 */
	public void setLocationInfo(boolean locationInfo) {
		this.locationInfo = locationInfo;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public void setReconnectionDelay(String delay) {
		reconnectionDelay = delay;
	}

	/**
	 * The <b>RemoteHost</b> option takes a string value which should be the
	 * host name of the server where a {@link SocketNode} is running.
	 */
	public void setRemoteHost(String host) {
		remoteHost = host;
	}

	public void setEnvironmentId(String environmentId) {
		this.environmentId = environmentId;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

}

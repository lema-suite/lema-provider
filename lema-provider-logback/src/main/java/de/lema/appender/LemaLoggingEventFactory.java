/*
  * #%L
 * lema-provider-logback
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.MDC;

import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.classic.spi.ThrowableProxy;

public class LemaLoggingEventFactory {

	public static LemaLoggingEvent create(LoggingEvent event, boolean locationInfo) {

		final String message = event.getFormattedMessage();

		final Throwable throwable = getThrowable(event);
		final String exceptionClass = getExceptionClass(throwable);
		final String exceptionMessage = getExceptionMessage(throwable);

		final long datum = event.getTimeStamp();
		final int level = event.getLevel().toInt();

		final StackTraceElement[] cda = getCallerData(event, locationInfo);
		final String className = getClass(event, cda);
		final String methodName = getMethodName(cda);
		final int lineNumber = getLineNumber(cda);

		final String threadName = event.getThreadName();
		final HashMap<String, String> mdc = createMdc();

		return new LemaLoggingEvent(message, mdc, throwable, null, datum, level, lineNumber, className, methodName, threadName, exceptionClass,
				exceptionMessage);

	}

	private static String getExceptionMessage(final Throwable throwable) {
		return (throwable != null) ? throwable.getMessage() : null;
	}

	private static String getExceptionClass(final Throwable throwable) {
		return (throwable != null) ?
				(throwable instanceof ExceptionHelper ?
						((ExceptionHelper) throwable).getClassName() :
						throwable.getClass()
						         .getName()) :
				null;
	}


	private static Throwable getThrowable(final LoggingEvent event) {
		return event.getThrowableProxy() != null ? ((ThrowableProxy) event.getThrowableProxy()).getThrowable() : null;
	}

	private static StackTraceElement[] getCallerData(final LoggingEvent event, final boolean locationInfo) {
		StackTraceElement[] cda = null;
		if (locationInfo) {
			cda = event.getCallerData();
		}
		return cda;
	}

	private static String getMethodName(final StackTraceElement[] cda) {
		return (cda != null && cda.length > 0) ? cda[0].getMethodName() : null;
	}

	private static int getLineNumber(final StackTraceElement[] cda) {
		return (cda != null && cda.length > 0) ? cda[0].getLineNumber() : 0;
	}


	private static HashMap<String, String> createMdc() {
		HashMap<String, String> mdc = new HashMap<>();

		Map<String, String> context = MDC.getCopyOfContextMap();
		if (context != null && !context.isEmpty()) {
			mdc = new HashMap<>();
			for (Entry<String, String> entry : context.entrySet()) {
				String key = entry.getKey();
				Object value = entry.getValue();
				if (key != null && value != null) {
					mdc.put(key, value.toString());
				}
			}
		}
		return mdc;
	}

	private static String getClass(LoggingEvent event, StackTraceElement[] cda) {
		if (cda != null && cda.length > 0) {
			return cda[0].getClassName();
		} else {
			return event.getLoggerName();
		}
	}
}

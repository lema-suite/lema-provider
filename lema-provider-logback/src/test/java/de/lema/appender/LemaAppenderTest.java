/*
 * #%L
 * lema-provider-logback
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyLong;

import org.junit.Test;
import org.mockito.Mockito;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.LoggingEvent;
import de.lema.appender.net.SocketThread;
import de.lema.appender.net.SocketThreadFactory;

public class LemaAppenderTest {

	@Test
	public void testStartAndStop()  {
		// setup
		final String port = "40001";

		SocketThreadFactory factory = Mockito.mock(SocketThreadFactory.class);
		SocketThread sender = Mockito.mock(SocketThread.class);

		Mockito.when(
				factory.createInstance(any(), any(), anyLong(), any(), anyBoolean(), anyInt(), anyInt())).thenReturn(sender);
		LemaAppender appender = new LemaAppender(factory);
		appender.setRemoteHost("localhost");
		appender.setPort(port);
		appender.setApplication("dummy");
		appender.setEnvironmentId("1");
		appender.setReconnectionDelay("1");
		LoggingEvent event = new Dummy();

		// Run

		appender.start();
		appender.append(event);

		appender.stop();

		Mockito.verify(sender).enqueForSending(any(LemaLoggingEvent.class));
		Mockito.verify(sender).cancel();

	}

	private static final class Dummy extends LoggingEvent {

		private static final long serialVersionUID = 1L;

		private Dummy() {
			super("Dummy", (new LoggerContext()).getLogger("dummy"), Level.INFO, "message", null, null);

		}
	}
}

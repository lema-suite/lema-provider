/*
 * #%L
 * lema-provider-common
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import junit.framework.Assert;

import org.junit.Test;

public class IdentifikationTest {

	@Test
	public void testSerialize() throws Exception {
		// Setup
		Identifikation expected = Identifikation.create("Test", 10, false,
				"1.0.0","",true);
		// Run

		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(expected);
		byte[] byteArray = bos.toByteArray();

		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
				byteArray));
		Identifikation actual = (Identifikation) ois.readObject();
		// Verify
		Assert.assertNotSame(expected, actual);
		Assert.assertEquals(expected, actual);
		Assert.assertEquals(expected.hashCode(), actual.hashCode());

	}

	@Test
	public void testAusgabe() throws Exception {
		// Setup
		String appVersion = "1.0.0";
		String anwendung = "Test";
		int umgebung = 10;
		Identifikation toTest = Identifikation.create(anwendung, umgebung,
				false, appVersion,"", true);

		String kurz = anwendung + "-" + umgebung;
		String logSenderVersion = Identifikation.getVersionFromFile();
		String zusatzTest = "LogVersion: " + logSenderVersion
				+ ", AppVersion: " + appVersion + ", Beacon: Nein";
		Assert.assertEquals(kurz, toTest.toStringKurz());
		Assert.assertEquals(zusatzTest, toTest.toStringZusatztext());
		Assert.assertEquals(kurz + " (" + zusatzTest + ")", toTest.toString());

	}
}

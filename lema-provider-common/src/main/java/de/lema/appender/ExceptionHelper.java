/*
 * #%L
 * lema-provider-common
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.List;

/**
 * In some cases (e.g. third-party-libary) an exception is only available as an
 * string. To log this via log4j to lema it has to be converted into an
 * exception. This class is reponsible for representing an "manuell" Throwble
 * constructed via String.
 * 
 */
public class ExceptionHelper extends Throwable {

	private static final long serialVersionUID = 1L;

	private final List<String> trace;

	private final String className;

	private final String exceptionMessage;

	public List<String> getTrace() {
		return trace;
	}

	public String getClassName() {
		return className;
	}

	public ExceptionHelper(String exceptionClass, String exceptionMessage,
			List<String> trace) {
		super();
		if (exceptionClass == null || trace == null || trace.size() < 1
				|| exceptionClass.length() < 1) {
			throw new IllegalArgumentException(
					"className oder trace war null oder leer");

		}
		this.className = exceptionClass;
		this.exceptionMessage = exceptionMessage;
		this.trace = trace;
	}

	public ExceptionHelper(String exceptionClass, List<String> trace) {
		this(exceptionClass, null, trace);
	}

	@Override
	public void printStackTrace(PrintWriter s) {
		synchronized (s) {
			s.println(className);
			for (String traceLine : trace) {
				s.println(removeNullAndBeginningTag(traceLine));
			}

		}
	}

	private String removeNullAndBeginningTag(String string) {
		if (string == null) {
			return "";
		} else {
			return string.startsWith("\t") && string.length() > 1 ? string
					.substring(1) : string;
		}
	}

	@Override
	public void printStackTrace(PrintStream s) {
		synchronized (s) {
			s.println(this.toString());
			for (String traceLine : trace) {
				s.println(removeNullAndBeginningTag(traceLine));
			}

		}
	}

	@Override
	public String toString() {
		return (exceptionMessage != null) ? (className + ": " + exceptionMessage)
				: className;
	}

	@Override
	public String getMessage() {
		return exceptionMessage;
	}

	@Override
	public synchronized Throwable fillInStackTrace() {
		return this;
	}

	@Override
	public Throwable getCause() {
		return null;
	}

	@Override
	public String getLocalizedMessage() {
		return exceptionMessage;
	}

	@Override
	public StackTraceElement[] getStackTrace() {

		return new StackTraceElement[0];
	}

	@Override
	public synchronized Throwable initCause(Throwable cause) {
		return this;
	}

	@Override
	public void printStackTrace() {
		printStackTrace(System.err);
	}

	@Override
	public void setStackTrace(StackTraceElement[] stackTrace) {
		// Wegwerfen
	}

}

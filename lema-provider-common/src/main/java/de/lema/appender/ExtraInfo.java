/*
 * #%L
 * lema-provider-common
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

public class ExtraInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public ExtraInfo() {
		props = new HashMap<String, String>();
		try {
			Properties properties = System.getProperties();

			Set<Entry<Object, Object>> entrySet = properties.entrySet();
			for (Entry<Object, Object> entry : entrySet) {
				Object value = entry.getValue();
				Object key = entry.getKey();
				if (value != null && key != null) {
					props.put(key.toString(), value.toString());
				}

			}
		} catch (SecurityException e) {
			// Kann passierern --> keine Info
		}
		maxMemory = Runtime.getRuntime().maxMemory();
		currentTimeMillis = System.currentTimeMillis();
	}

	public ExtraInfo(long maxMemory, long currentTimeMillis,
			HashMap<String, String> props) {
		this.maxMemory = maxMemory;
		this.currentTimeMillis = currentTimeMillis;
		this.props = props;
	}

	private final long maxMemory;
	private final long currentTimeMillis;
	private final HashMap<String, String> props;

	public long getMaxMemory() {
		return maxMemory;
	}

	public long getCurrentTimeMillis() {
		return currentTimeMillis;
	}

	public HashMap<String, String> getProps() {
		return props;
	}

	public static void main(String[] args) {
		ExtraInfo extraInfo = new ExtraInfo();
		for (Entry<String, String> entry : extraInfo.getProps().entrySet()) {
			System.out.println(entry.getKey() + ": " + entry.getValue());
		}
		System.out.println("Mem: " + extraInfo.getMaxMemory());
		System.out.println("Time: " + extraInfo.getCurrentTimeMillis());
	}

	public static final class SerializationProxy implements Serializable {

		private static final long serialVersionUID = 12345L;
		private transient ExtraInfo extraInfo;

		public SerializationProxy(ExtraInfo extraInfo) {
			this.extraInfo = extraInfo;
		}

		private void writeObject(ObjectOutputStream s) throws IOException {
			s.defaultWriteObject();
			s.writeInt(3);
			s.writeObject(extraInfo.getProps());
			s.writeLong(extraInfo.getCurrentTimeMillis());
			s.writeLong(extraInfo.getMaxMemory());
		}

		private void readObject(ObjectInputStream s) throws IOException,
				ClassNotFoundException {
			s.defaultReadObject();
			final int felderImStrom = s.readInt();
			final HashMap<String, String> props = read(s, felderImStrom, 1,
					new HashMap<String, String>());
			final long currentTimeInMillies = read(s, felderImStrom, 2, 0);
			final long maxMem = read(s, felderImStrom, 3, 0);
			extraInfo = new ExtraInfo(maxMem, currentTimeInMillies, props);
		}

		private Object readResolve() {
			return extraInfo;
		}

		@SuppressWarnings("unchecked")
		private <T> T read(ObjectInputStream s, final int felderImStrom,
				int akt, T def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? (T) s.readObject() : def;
		}

		private long read(ObjectInputStream s, final int felderImStrom,
				int akt, long def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? s.readLong() : def;
		}

	}

	private Object writeReplace() {
		return new SerializationProxy(this);
	}

	private void readObject(ObjectInputStream stream)
			throws InvalidObjectException {
		throw new InvalidObjectException("Proxy required");
	}

}

/*
 * #%L
 * lema-provider-common
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;


public class LemaLoggingEvent implements Serializable {

	public String getMessage() {
		return message;
	}

	private static final long serialVersionUID = 12345L;

	protected static final int MAX = 1000000;
	
	private final String message;
	private final HashMap<String, String> mdcMap;
	private final String traceString;
	private final Throwable trace;
	private final long datum;
	private final int level;
	private final int lineNumber;
	private final String className;
	private final String methodName;
	private final String threadName;

	private final String exceptionKlasse;

	private int failureCounter;
	
	private static boolean removeFirstLine = true;

	public String getTraceString() {
		return traceString;
	}

	private final String exceptionMessage;

	public LemaLoggingEvent(String message, HashMap<String, String> mdc, Throwable trace, String traceString, long datum, int level, int lineNumber,
			String className, String methodName, String threadName, String exceptionKlasse, String exceptionMessage) {
		super();
		this.message = message;
		this.mdcMap = mdc;
		this.trace = trace;
		this.datum = datum;
		this.level = level;
		this.lineNumber = lineNumber;
		this.className = className;
		this.methodName = setToNullIfEmpty(methodName);
		this.threadName = setToNullIfEmpty(threadName);
		this.exceptionKlasse = setToNullIfEmpty(exceptionKlasse);
		this.exceptionMessage = setToNullIfEmpty(exceptionMessage);
		this.traceString = setToNullIfEmpty(traceString);


	}

	private String setToNullIfEmpty(String s) {
		return (s != null && s.length() > 0) ? s : null;
	}

	public LemaLoggingEvent(String message, int level, String className) {
		super();
		this.message = message;
		mdcMap = null;
		trace = null;
		datum = System.currentTimeMillis();
		this.level = level;
		lineNumber = 0;
		this.className = className;
		methodName = null;
		threadName = Thread.currentThread().getName();
		exceptionKlasse = null;
		exceptionMessage = null;
		traceString = null;
		this.removeFirstLine = true;
	}

	public HashMap<String, String> getMdcMap() {
		return mdcMap;
	}

	public Throwable getTrace() {
		return trace;
	}

	public long getDatum() {
		return datum;
	}

	public int getLevel() {
		return level;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public String getClassName() {
		return className;
	}

	public String getMethodName() {
		return methodName;
	}

	public String getThreadName() {
		return threadName;
	}

	public String getExceptionKlasse() {
		return exceptionKlasse;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public static String getTrace(Throwable throwable) {
		if (throwable instanceof ExceptionHelper) {
			return extractStringExceptionHelper((ExceptionHelper) throwable);
		} else {
			try {

				TraceBuilder tb = new TraceBuilder(removeFirstLine);
				throwable.printStackTrace(tb);

				return tb.toString();
			} catch (RuntimeException ex) {
				ex.printStackTrace();
				return null;
			}
		}

	}

	private static String extractStringExceptionHelper(ExceptionHelper helper) {

		StringBuilder sb = new StringBuilder();
		List<String> traceList = helper.getTrace();
		for (int i = 0; i < traceList.size() && sb.length() < MAX; i++) {

			sb.append(traceList.get(i));
			if (i < traceList.size() - 1) {
				sb.append("\n");
			}
		}

		return sb.toString();
	}

	private static class TraceBuilder extends PrintWriter {

		private final StringBuilder v;
		boolean firstLine = true;

		public String toString() {
			return v.toString();
		}

		TraceBuilder(boolean removeFirstLine) {
			super(new NullWriter());
			v = new StringBuilder();
			this.firstLine = removeFirstLine;
		}

		public void print(Object o) {
			handleString(o.toString());
		}

		private void handleString(String o) {
			if (firstLine) {
				firstLine = false;
			} else {
				final int length = v.length();
				if (length < MAX) {

					if (checkLineBegin(o)) {
						o = o.substring(1);
					}
					if (length + o.length() > MAX) {
						o = o.substring(checkLineBegin(o) ? 1 : 0, MAX - length);

					}
					v.append(o).append("\n");
				}
			}
		}

		private boolean checkLineBegin(String o) {
			return o.startsWith("\t") && o.length() > 1;
		}

		public void print(char[] chars) {
			handleString(new String(chars));
		}

		public void print(String s) {
			handleString(s);
		}

		public void println(Object o) {
			handleString(o.toString());
		}

		public void println(char[] chars) {
			handleString(new String(chars));
		}

		public void println(String s) {
			handleString(s);
		}

		public void write(char[] chars) {
			handleString(new String(chars));
		}

		public void write(char[] chars, int off, int len) {
			handleString(new String(chars, off, len));
		}

		public void write(String s, int off, int len) {
			handleString(s.substring(off, off + len));
		}

		public void write(String s) {
			handleString(s);
		}

	}

	private static class NullWriter extends Writer {

		public void close() {

		}

		public void flush() {

		}

		public void write(char[] cbuf, int off, int len) {

		}
	}

	public static final class SerializationProxy implements Serializable {

		private static final long serialVersionUID = 12345L;
		private transient LemaLoggingEvent event;

		public SerializationProxy(LemaLoggingEvent event) {
			this.event = event;
		}

		private void writeObject(ObjectOutputStream s) throws IOException {
			s.defaultWriteObject();
			s.writeInt(11);
			s.writeInt(event.getLevel());
			s.writeLong(event.getDatum());
			s.writeObject(event.getClassName());
			s.writeObject(event.getThreadName());
			s.writeObject(event.getMessage());
			s.writeObject(event.getMethodName());
			s.writeInt(event.getLineNumber());
			s.writeObject(event.getMdcMap());
			s.writeObject(event.getExceptionKlasse());
			s.writeObject(event.getExceptionMessage());
			s.writeObject(event.getTrace() != null ? LemaLoggingEvent.getTrace(event.getTrace()) : null);

		}

		private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
			s.defaultReadObject();
			final int felderImStrom = s.readInt();

			final int level = read(s, felderImStrom, 1, 0);
			final long datum = read(s, felderImStrom, 2, System.currentTimeMillis());
			final String className = read(s, felderImStrom, 3, "LemaLogger");
			final String threadName = read(s, felderImStrom, 4, null);
			final String message = read(s, felderImStrom, 5, null);
			final String methodName = read(s, felderImStrom, 6, null);
			final int lineNumber = read(s, felderImStrom, 7, 0);
			final HashMap<String, String> mdc = read(s, felderImStrom, 8, (HashMap<String, String>) null);
			final String exceptionKlasse = read(s, felderImStrom, 9, null);
			final String exceptionMessage = read(s, felderImStrom, 10, null);
			final String traceString = read(s, felderImStrom, 11, null);

			event = new LemaLoggingEvent(message, mdc, null, traceString, datum, level, lineNumber, className, methodName, threadName,
					exceptionKlasse, exceptionMessage);
		}

		private Object readResolve() {
			return event;
		}

		private String read(ObjectInputStream s, final int felderImStrom, int akt, String def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? (String) s.readObject() : def;
		}

		private int read(ObjectInputStream s, final int felderImStrom, int akt, int def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? s.readInt() : def;
		}

		private long read(ObjectInputStream s, final int felderImStrom, int akt, long def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? s.readLong() : def;
		}

		@SuppressWarnings("unchecked")
		private <T> T read(ObjectInputStream s, final int felderImStrom, int akt, T def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? (T) s.readObject() : def;
		}

	}

	private Object writeReplace() {
		return new SerializationProxy(this);
	}

	private void readObject(ObjectInputStream stream) throws InvalidObjectException {
		throw new InvalidObjectException("Proxy required");
	}
	
	public int registerFailure(){
		return ++failureCounter;
	}

	public int getFailureCounter() {
		return failureCounter;
	}
}

/*
 * #%L
 * lema-provider-common
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.RejectedExecutionException;


import de.lema.annotations.Immutable;

/**
 * Identifikation des Log-Clients am Server. Damit die Attribute erweitert
 * werden koennen, wird ein SerialisationProxy benutzt.
 */
@Immutable
public final class Identifikation implements Serializable {

	private static final long serialVersionUID = 12345L;

	public static Identifikation create(String anwendung, int umgebung, boolean hatBeacon, String appversion, String hostname, boolean mitExtraInfo) {
		final String logVersion = getVersionFromFile();
		final ExtraInfo extraInfo = (mitExtraInfo) ? new ExtraInfo() : null;
		return createManuell(anwendung, umgebung, logVersion, hatBeacon, appversion, hostname, extraInfo);
	}

	public static Identifikation createManuell(String anwendung, int umgebung, String logVersion, boolean hatBeacon, String appversion, String hostname,
			ExtraInfo extraInfo) {

		return new Identifikation(anwendung, umgebung, logVersion, hatBeacon, appversion, hostname, extraInfo);
	}

	private final String anwendung;

	public String getLogSenderVersion() {
		return this.logSenderVersion;
	}

	private final String logSenderVersion;

	private final int umgebung;

	private final boolean hatBeacon;

	private final String applikationVersion;

	private final ExtraInfo extraInfo;

	private final String hostname;

	/**
	 * @return the hatBeacon
	 */
	public boolean isHatBeacon() {
		return this.hatBeacon;
	}

	public ExtraInfo getExtraInfo() {
		return this.extraInfo;
	}

	private Identifikation(String anwendung, int umgebung, String logSenderVersion, boolean hatBeacon, String applikationVersion, String hostname,
			ExtraInfo extraInfo) {
		this.anwendung = anwendung;
		this.umgebung = umgebung;
		this.logSenderVersion = logSenderVersion;
		this.hatBeacon = hatBeacon;
		this.applikationVersion = applikationVersion;
		this.hostname = hostname;
		this.extraInfo = extraInfo;
	}

	public String getHostname() {
		return this.hostname;
	}

	/**
	 * @return the applikationVersion
	 */
	public String getApplikationVersion() {
		return this.applikationVersion;
	}

	public String getAnwendung() {
		return this.anwendung;
	}

	public int getUmgebung() {
		return this.umgebung;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.anwendung == null) ? 0 : this.anwendung.hashCode());
		result = prime * result + ((this.applikationVersion == null) ? 0 : this.applikationVersion.hashCode());
		result = prime * result + (this.hatBeacon ? 1231 : 1237);
		result = prime * result + ((this.logSenderVersion == null) ? 0 : this.logSenderVersion.hashCode());
		result = prime * result + this.umgebung;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Identifikation)) {
			return false;
		}
		Identifikation other = (Identifikation) obj;
		if (this.anwendung == null) {
			if (other.anwendung != null) {
				return false;
			}
		} else if (!this.anwendung.equals(other.anwendung)) {
			return false;
		}
		if (this.applikationVersion == null) {
			if (other.applikationVersion != null) {
				return false;
			}
		} else if (!this.applikationVersion.equals(other.applikationVersion)) {
			return false;
		}
		if (this.hatBeacon != other.hatBeacon) {
			return false;
		}
		if (this.logSenderVersion == null) {
			if (other.logSenderVersion != null) {
				return false;
			}
		} else if (!this.logSenderVersion.equals(other.logSenderVersion)) {
			return false;
		}
		if (this.umgebung != other.umgebung) {
			return false;
		}
		return true;
	}

	public boolean istValid() {
		return this.getAnwendung() != null && this.getAnwendung()
		                                          .length() > 0;
	}

	public static String getVersionFromFile() {
		try {
			Properties props = new Properties();
			InputStream inputStream = Identifikation.class.getClassLoader()
			                                              .getResourceAsStream("generated.properties");

			props.load(inputStream);
			String version = props.getProperty("version");

			inputStream.close();
			return version;
		} catch (Exception exc) {
			return "unbekannt";
		}
	}

	/**
	 * Diese Implementierung darf nicht geaendert werden. Dieser String dient
	 * zur Intentifikation der Instanz
	 */
	public String toStringKurz() {
		return this.anwendung + "-" + this.umgebung;
	}

	@Override
	public String toString() {
		return this.toStringKurz() + " (" + this.toStringZusatztext() + ")";
	}

	public String toStringZusatztext() {
		return "LogVersion: " + this.logSenderVersion + ", AppVersion: " + this.applikationVersion + ", Beacon: " + (this.hatBeacon ? "Ja" : "Nein");
	}

	public static final class SerializationProxy implements Serializable {

		private static final long serialVersionUID = 12345L;
		private transient Identifikation identifikation;

		public SerializationProxy(Identifikation identifikation) {
			this.identifikation = identifikation;
		}

		private void writeObject(ObjectOutputStream s) throws IOException {
			s.defaultWriteObject();
			s.writeInt(7);
			s.writeObject(this.identifikation.getAnwendung());
			s.writeInt(this.identifikation.getUmgebung());
			s.writeObject(this.identifikation.getLogSenderVersion());
			s.writeBoolean(this.identifikation.isHatBeacon());
			s.writeObject(this.identifikation.getApplikationVersion());
			s.writeObject(this.identifikation.getExtraInfo());
			s.writeObject(this.identifikation.getHostname());
		}

		private void readObject(ObjectInputStream s) throws IOException, ClassNotFoundException {
			s.defaultReadObject();
			final int felderImStrom = s.readInt();
			final String anwendung = this.read(s, felderImStrom, 1, "<Anwendung>");
			final int umgebung = this.read(s, felderImStrom, 2, 10);
			final String logVersion = this.read(s, felderImStrom, 3, "<LogServerVersion>");
			final boolean hatBecon = this.read(s, felderImStrom, 4, false);
			final String appVersion = this.read(s, felderImStrom, 5, "<ApplikationVersion>");
			final ExtraInfo extraInfo = this.read(s, felderImStrom, 6, (ExtraInfo) null);
			final String hostname = this.read(s, felderImStrom, 7, "");
			this.identifikation = new Identifikation(anwendung, umgebung, logVersion, hatBecon, appVersion, hostname, extraInfo);
		}

		private Object readResolve() {
			return this.identifikation;
		}

		private String read(ObjectInputStream s, final int felderImStrom, int akt, String def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? (String) s.readObject() : def;
		}

		private int read(ObjectInputStream s, final int felderImStrom, int akt, int def) throws IOException {
			return (felderImStrom >= akt) ? s.readInt() : def;
		}

		private boolean read(ObjectInputStream s, final int felderImStrom, int akt, boolean def) throws IOException {
			return (felderImStrom >= akt) ? s.readBoolean() : def;
		}

		@SuppressWarnings("unchecked")
		private <T> T read(ObjectInputStream s, final int felderImStrom, int akt, T def) throws IOException, ClassNotFoundException {
			return (felderImStrom >= akt) ? (T) s.readObject() : def;
		}

	}

	private Object writeReplace() {
		return new SerializationProxy(this);
	}

	private void readObject(ObjectInputStream stream) throws InvalidObjectException {
		throw new InvalidObjectException("Proxy required");
	}
}

/*
 * #%L
 * lema-provider-jul
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class LemaLoggingEventFactory {

	public static LemaLoggingEvent create(LogRecord event) {

		String message = event.getMessage();

		Throwable throwable = event.getThrown();
		final String exceptionKlasse = (throwable != null) ? (throwable instanceof ExceptionHelper ? ((ExceptionHelper) throwable).getClassName()
				: throwable.getClass().getName()) : null;
		final String exceptionMessage = (throwable != null) ? throwable.getMessage() : null;

		long datum = event.getMillis();
		int level = convertLevel(event.getLevel());

		String className = event.getSourceClassName();
		String methodName = event.getSourceMethodName();
		int lineNumber = 0;

		String threadName = Thread.currentThread().getName();
		HashMap<String, String> mdc = null;

		return new LemaLoggingEvent(message, mdc, throwable, null, datum, level, lineNumber, className, methodName, threadName, exceptionKlasse,
				exceptionMessage);

	}

	private static int convertLevel(final Level level) {

		int levelInt = level.intValue();
		if (levelInt == Level.SEVERE.intValue()) {
			return 40000;
		} else if (levelInt == Level.WARNING.intValue()) {
			return 30000;
		} else if (levelInt == Level.INFO.intValue()) {
			return 20000;
		} else if (levelInt == Level.FINE.intValue()) {
			return 10000;
		} else if (levelInt == Level.FINER.intValue()) {
			return 5000;
		} else {
			return 0;
		}
	}

}

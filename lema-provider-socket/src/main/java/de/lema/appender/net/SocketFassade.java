/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import de.lema.annotations.GuardedBy;
import de.lema.annotations.ThreadSafe;

@ThreadSafe
public class SocketFassade {

	private static final int TIMEOUT = 10000;

	private static final AtomicInteger errorCounter = new AtomicInteger(0);

	/**
	 * Erzeugt eine Verbindung.
	 * 
	 * @param data
	 * @return null im Fehlerfall sonst die bestehende Verbindung, bei der die
	 *         Identifikation erfolgreich verschickt wurde.
	 */
	public static SocketFassade create(ConnectionProperties data) {
		try {
			final SocketFassade socketFassade = new SocketFassade(data);
			errorCounter.set(0);
			return socketFassade;
		} catch (ConnectException e) {
		//	LogLog.error("Try " + errorCounter.incrementAndGet() + ". No connection to " + data.getHost() + ":" + data.getPort());
			return null;
		} catch (SocketTimeoutException e) {
			//	LogLog.error("Try " + errorCounter.incrementAndGet() + ". Servertimeout to " + data.getHost() + ":" + data.getPort());
			return null;
		} catch (ConnectionClosedException e) {
			//	LogLog.error("Try " + errorCounter.incrementAndGet() + ". Connection closes on write (normally too many connections) to "
			//		+ data.getHost() + ":" + data.getPort());
			return null;

		} catch (IOException e) {
			//	LogLog.error("Try " + errorCounter.incrementAndGet() + ". Connection-Error to " + data.getHost() + ":" + data.getPort(), e);
			return null;
		}
	}

	private volatile boolean istClosed;

	@GuardedBy(lock = "ois")
	private final ObjectInputStream ois;

	@GuardedBy(lock = "oos")
	private final ObjectOutputStream oos;

	@GuardedBy(lock = "this")
	private final Socket socket;

	private SocketFassade(ConnectionProperties data) throws IOException {
		super();
		socket = new Socket(data.getAddress(), data.getPort());
		socket.setSoTimeout(TIMEOUT);
		oos = new ObjectOutputStream(socket.getOutputStream());
		ois = new ObjectInputStream(socket.getInputStream());
		final boolean ok = write(data.getIdent());
		if (!ok) {
			throw new ConnectionClosedException();
		}

	}

	/**
	 * Wir wollen nicht mehrere Thread gleichzeitg schlie�en lassen.
	 */
	public void close() {
		synchronized (this) {

			if (!istClosed) {
				// Er wird sofort als Geschlossen deklariert
				istClosed = true;
				try {
					ois.close();

				} catch (Exception e) {

				}

				try {
					oos.close();

				} catch (Exception e) {

				}

				try {
					socket.close();
				} catch (Exception e) {

				}

			}
		}

	}

	/**
	 * @return the istClosed
	 */
	public boolean istClosed() {
		return istClosed;
	}

	/**
	 * Im Fehlerfall wird die Verbindung geschlossen.
	 */
	public Object read() throws Exception {
		synchronized (ois) {
			if (!istClosed) {
				try {

					Object readObject = ois.readObject();
					return readObject;

				} catch (Exception e) {
					close();
					throw e;
				}
			} else {
				throw new Exception("Verbindung ist schon geschlossen.");
			}
		}

	}

	/**
	 * Im Fehlerfall wird die Verbindung geschlossen.
	 * 
	 * @return true Ok, false sonst.
	 */
	public boolean write(Object tosend) {
		synchronized (oos) {
			if (!istClosed) {
				try {

					oos.writeObject(tosend);
					oos.flush();
					oos.reset();
					return true;

				} catch (Exception e) {
					close();
					return false;
				}
			} else {
				return false;
			}
		}
	}

}

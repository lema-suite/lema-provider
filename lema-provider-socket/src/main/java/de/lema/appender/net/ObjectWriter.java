/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import de.lema.annotations.NotThreadSafe;

/**
 * Der Writer h�ngt Object an eine Datei an. Er darf nur von einem Thread
 * verwendet werden und es darf nicht gleichzeitig an die Datei angeh�ngt
 * werden!
 */
@NotThreadSafe
public class ObjectWriter<T> extends AbstractObjectWriter {

	private final File dir;
	private final int maxElementeImHeader;
	private HeaderElement headerElementAkt;

	public ObjectWriter(int headerLenght, File dir, String applikation) {
		if (dir.isFile() || !dir.exists()) {
			throw new IllegalArgumentException(
					"Verzeichnis existiert nicht oder ist eine Datei.");
		}
		if (headerLenght < 1) {
			throw new IllegalArgumentException(
					"Die Headergroesse muss groesser 0 sein, war "
							+ headerLenght);
		}
		this.dir = dir;
		this.random = new Random(System.currentTimeMillis());
		this.applikation = applikation;
		this.maxElementeImHeader = headerLenght;

	}

	private final Random random;

	private int createRandomNumber() {
		return random.nextInt(1000000);
	}

	private static final SimpleDateFormat SDF = new SimpleDateFormat(
			"yyyy-MM-dd-HH-mm-ss-SSS");

	private final String applikation;

	private File generateFile() throws IOException {
		final Date jetzt = new Date(System.currentTimeMillis());
		int random = createRandomNumber();
		File file = new File(dir.getAbsolutePath() + File.separator + "log-"
				+ applikation + "-" + SDF.format(jetzt) + "-" + random
				+ ".data");
		boolean ok = file.createNewFile();
		if (!ok) {
			throw new IllegalStateException(
					"Die Datei konnte nicht angelegt werden bzw. existiert bereits.");
		}

		return file;
	}

	private void createNewFile() throws IOException {
		openFile(generateFile());
		writeEmptyHeader(maxElementeImHeader);
	}

	public long write(T o) throws IOException {
		if (o == null) {
			throw new IllegalArgumentException("Object war null");
		}

		if (!isFileOpen()) {
			createNewFile();
			headerElementAkt = new HeaderElement(
					berechneErsteSchreibposition(maxElementeImHeader), 0, -1);
		}

		final byte[] serializeObject = ObjectSerializer.serializeObject(o);

		final int length = serializeObject.length;
		headerElementAkt = new HeaderElement(
				headerElementAkt.getStartPosImBody()
						+ headerElementAkt.getLengthImBody(),
				serializeObject.length,
				headerElementAkt.getElementNummerImHeader() + 1);

		writeAndUpdateHeader(headerElementAkt, serializeObject,
				maxElementeImHeader);
		// Nacharbeiten
		if (headerElementAkt == null
				|| headerElementAkt.getElementNummerImHeader() >= maxElementeImHeader - 1) {
			close();

		}
		return headerElementAkt.getStartPosImBody() + length;

	}

}

/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.Serializable;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import de.lema.annotations.ThreadSafe;
import de.lema.appender.LemaLoggingEvent;

@ThreadSafe
public class SocketThread extends Thread {

	private static final long HOUR_IN_MILLIS = 3600000L;

	private final long reconnectionDelay;

	private final Factory<SocketFassade> socketProvider;

	private final CountDownLatch startGate;

	private final int connectOnDemandDisconnectTime;

	private final boolean connectOnDemand;

	private final ArrayBlockingQueue<LemaLoggingEvent> queue;

	private final long beaconFix = 120;

	private final int beaconVar = 60;

	private final Random random;

	private int unsuccessfullConnectionCount;
	private boolean firstConnectSucessfullyFinisched;
	private SocketFassade socketFassade;

	private volatile boolean running;

	private final Object lock = new Object();

	/**
	 * Wichtig! Die Clienten duerfen das Gate nicht herunterzaehlen
	 *
	 * @return the startGate
	 */
	CountDownLatch getStartGate() {
		return this.startGate;
	}

	SocketThread(String name, long reconnectionDelay, Factory<SocketFassade> socketProvider, boolean connectOnDemand, int connectOnDemandDisconnectTime,
			int bufferSize) {
		super("LemaThread" + ((name != null && name.length() > 0) ? ("-" + name) : ""));
		this.connectOnDemand = connectOnDemand;
		this.connectOnDemandDisconnectTime = connectOnDemandDisconnectTime;
		this.startGate = new CountDownLatch(1);
		this.socketProvider = socketProvider;
		this.setDaemon(true);
		this.setPriority(Thread.MIN_PRIORITY);
		this.running = true;
		this.reconnectionDelay = reconnectionDelay;

		this.queue = new ArrayBlockingQueue<>(bufferSize);
		this.random = new Random(System.currentTimeMillis());
	}

	private boolean connect() {

		try {
			SocketFassade create = this.socketProvider.get();
			if (create != null) {
				this.setSocket(create);
				this.unsuccessfullConnectionCount = 0;
				return true;
			} else {
				this.unsuccessfullConnectionCount++;
				return false;
			}
		} catch (Exception e) {
			//LogLog.error("Fehler beim Verbindungsaufbau", e);
			this.unsuccessfullConnectionCount++;
			return false;
		}

	}

	private void setSocket(SocketFassade neu) {
		final SocketFassade old = this.getSocketFassade();
		if (old != null) {
			old.close();
		}

		this.socketFassade = neu;
		if (neu != null) {
			if (this.firstConnectSucessfullyFinisched) {
				this.afterReconnect();
			} else {
				this.startupFinished();
			}
		}
	}

	private void afterReconnect() {

	}

	private void startupFinished() {
		this.firstConnectSucessfullyFinisched = true;
		this.startGate.countDown();
	}

	SocketFassade getSocketFassade() {

		if (this.socketFassade == null) {
			return null;
		} else {
			if (this.socketFassade.istClosed()) {
				this.socketFassade = null;
				return null;
			} else {
				return this.socketFassade;
			}
		}
	}

	public boolean isRunning() {
		return this.running;
	}

	@Override
	public void run() {
		try {
			if (this.connectOnDemand) {
				this.runOnDemand();
			} else {
				this.runBeacon();
			}
		} catch (InterruptedException e) {
			// Wir duerfen es schlucken, da dies zur Cancelstrategie
			// gehoert!
			this.running = false;
		}

	}

	private void runBeacon() throws InterruptedException {
		this.connect();

		while (this.running) {
			if (this.getSocketFassade() != null) {
				Serializable take = this.queue.poll(this.beaconFix + this.random.nextInt(this.beaconVar), TimeUnit.SECONDS);
				if (take == null) {

					boolean ok = this.write(Beacon.BEACON);
					// Beim Beacon muessen wir auf die Antwort warten
					if (ok) {
						this.read();
					}

				} else {
					this.write(take);
				}

			} else {
				sleep(this.calcSleepTime());
				this.connect();
			}

		}
	}

	private void runOnDemand() throws InterruptedException {
		this.startupFinished(); // manuell ausloesen
		while (this.running) {

			Serializable take = (this.getSocketFassade() == null) ? this.queue.take() : this.queue.poll(this.connectOnDemandDisconnectTime,
			                                                                                            TimeUnit.MILLISECONDS);

			// Daten
			if (take != null) {

				// Daten
				if (this.getSocketFassade() == null) {
					this.connect();
				}

				boolean ok = this.write(take);
				// Im Fehlerfall warten, denn die Daten stellen sich wieder
				// hinten an!
				if (!ok) {
					sleep(this.calcSleepTime());
				}

			} else {
				// Timeout
				// Keine Daten (mehr)! --> Schliessen!
				this.setSocket(null);
			}

		}
	}

	private long calcSleepTime() {
		long time = this.reconnectionDelay;
		for (int i = 0; i < this.unsuccessfullConnectionCount; i++) {
			if (time > HOUR_IN_MILLIS) {
				time = (time * 3) / 2;
			} else {
				time = time * 2;
			}

		}

		return time;
	}

	public void cancel() {
		this.running = false;
		this.interrupt();
		synchronized (this.lock) {
			this.setSocket(null);
		}

	}

	Object read() {

		final SocketFassade instance = this.getSocketFassade();

		if (instance != null) {
			try {
				return instance.read();
			} catch (Exception e) {
				this.deregister(instance);

			}
		}

		return null;
	}

	boolean write(final Serializable toSend) {

		final SocketFassade instance = this.getSocketFassade();
		boolean ok = false;
		if (instance != null) {
			ok = instance.write(toSend);
			if (!ok) {
				this.deregister(instance);

			}

		}

		if (!ok && toSend instanceof LemaLoggingEvent) {
			this.onFailure((LemaLoggingEvent) toSend);
		}
		return ok;

	}

	private void onFailure(LemaLoggingEvent toSend) {

		if (toSend.registerFailure() < 10) {
			this.enqueForSending(toSend);
		} else {
			this.onError(toSend);
		}

	}

	private void deregister(final SocketFassade instance) {
		instance.close();
		this.socketFassade = null;
	}

	/**
	 * Schreibt ein Event in die Sender Queue. Wenn die Queue voll ist, wird ein
	 * Element vorne entfernt und der ConnectionLostStrategie gemeldet.
	 */
	public void enqueForSending(LemaLoggingEvent event) {
		if (this.running) {
			// Schleife ist noetig, da die Methode nicht synchronisiert ist.
			while (!this.queue.offer(event)) {
				final LemaLoggingEvent poll = this.queue.poll();
				if (poll != null) {
					this.onOverflow( poll);
				}

			}
		}
	}

	public void onOverflow(LemaLoggingEvent poll) {
		// Muell

	}

	public void onError(LemaLoggingEvent poll) {
		// Muell

	}

	public boolean isEmpty() {
		return this.queue.isEmpty();
	}

	static SocketThread createInstance(String name, final Factory<SocketFassade> provider, long reconnectionDelay, boolean connectOnDemand,
			int connectOnDemandDisconnectTime, int bufferSize) {

		SocketThread instance = new SocketThread(name, reconnectionDelay, provider, connectOnDemand, connectOnDemandDisconnectTime, bufferSize);
		instance.start();
		return instance;

	}

}

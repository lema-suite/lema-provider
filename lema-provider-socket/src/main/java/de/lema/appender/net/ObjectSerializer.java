/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class ObjectSerializer {

	private final static class ByteArrayOutputStreamExtension extends
			ByteArrayOutputStream {

		public byte[] getBufferdirect() {
			byte newbuf[] = new byte[count];
			System.arraycopy(buf, 0, newbuf, 0, count);
			return newbuf;
		}
	}

	public static Object deserialize(byte[] in) throws IOException,
			ClassNotFoundException {
		Object back;
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
				in));
		back = ois.readObject();
		return back;
	}

	public static byte[] serializeObject(Object o) throws IOException {
		ByteArrayOutputStreamExtension bos = new ByteArrayOutputStreamExtension();
		ObjectOutputStream oos = new ObjectOutputStream(bos);
		oos.writeObject(o);
		oos.flush();

		final byte[] out = bos.getBufferdirect();
		oos.close();
		bos.close();
		return out;
	}
}

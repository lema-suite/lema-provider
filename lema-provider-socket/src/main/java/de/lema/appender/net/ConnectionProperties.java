/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.net.InetAddress;

import de.lema.annotations.Immutable;
import de.lema.appender.Identifikation;

@Immutable
public class ConnectionProperties {
	private static InetAddress getAddressByName(String host) {
		try {
			return InetAddress.getByName(host);
		} catch (Exception e) {
			//LogLog.error("Could not find address of [" + host + "].", e);
			return null;
		}
	}

	private final InetAddress address;

	private final String host;

	private final Identifikation ident;

	private final int port;

	public ConnectionProperties(Identifikation ident, String host, int port) {

		this.ident = ident;
		this.host = host;
		this.port = port;

		address = getAddressByName(host);
	}

	/**
	 * @return the address
	 */
	public InetAddress getAddress() {
		return address;
	}

	public String getHost() {
		return host;
	}

	public Identifikation getIdent() {
		return ident;
	}

	public int getPort() {
		return port;
	}

	public boolean istValid() {
		return address != null;
	}

}

/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import de.lema.appender.Identifikation;

public class SocketThreadFactory {

	public static final SocketThreadFactory INSTANCE = new SocketThreadFactory();

	private final HashMap<Instanz, SocketThread> MAP = new HashMap<Instanz, SocketThread>();

	private SocketThreadFactory() {

	}

	private static class Instanz {

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((anwendung == null) ? 0 : anwendung.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + umgebung;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			Instanz other = (Instanz) obj;
			if (anwendung == null) {
				if (other.anwendung != null) {
					return false;
				}
			} else if (!anwendung.equals(other.anwendung)) {
				return false;
			}
			if (name == null) {
				if (other.name != null) {
					return false;
				}
			} else if (!name.equals(other.name)) {
				return false;
			}
			if (umgebung != other.umgebung) {
				return false;
			}
			return true;
		}

		private final String name;
		private final String anwendung;
		private final int umgebung;

		@Override
		public String toString() {
			return "" + anwendung + "-" + umgebung + ((name != null && name.length() > 0) ? "-" + name : "");
		}

		public Instanz(String anwendung, int umgebung, String name) {
			super();
			this.anwendung = anwendung;
			this.umgebung = umgebung;
			this.name = name;
		}

	}

	public synchronized SocketThread createInstance(String name, final ConnectionProperties props, long reconnectionDelay, Identifikation ident,
			boolean connectOnDemand, int connectOnDemandDisconnectTime, int bufferSize) {

		final Instanz key = new Instanz(ident.getAnwendung(), ident.getUmgebung(), name);
		close(key);
		final SocketThread value = SocketThread.createInstance(key.toString(),
		                                                       new SocketFassadeFactory(props),
		                                                       reconnectionDelay,
		                                                       connectOnDemand,
		                                                       connectOnDemandDisconnectTime, bufferSize);
		MAP.put(key, value);
		return value;

	}

	private static class SocketFassadeFactory implements Factory<SocketFassade> {

		private final ConnectionProperties connectionProperties;

		public SocketFassadeFactory(ConnectionProperties connectionProperties) {
			this.connectionProperties = connectionProperties;

		}

		public SocketFassade get() {
			if (connectionProperties.istValid()) {
				return SocketFassade.create(connectionProperties);
			} else {
				return null;
			}
		}

	}

	public synchronized void close(String anwendung, int umgebung, String name) {
		close(new Instanz(anwendung, umgebung, name));

	}

	public synchronized void closeAll() {
		Set<Entry<Instanz, SocketThread>> entrySet = MAP.entrySet();
		for (Entry<Instanz, SocketThread> entry : entrySet) {
			SocketThread value = entry.getValue();
			if (value != null) {
				value.cancel();
			}
		}
		MAP.clear();
	}

	private synchronized void close(Instanz key) {
		SocketThread old = MAP.remove(key);

		if (old != null) {
			old.cancel();
		}

	}

}

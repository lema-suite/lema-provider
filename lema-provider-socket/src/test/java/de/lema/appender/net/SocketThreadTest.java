/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import de.lema.appender.LemaLoggingEvent;
import junit.framework.Assert;


@RunWith(MockitoJUnitRunner.class)
public class SocketThreadTest {

	SocketThread toTest;

	@Mock
	SocketFassade socketFassade;

	@Mock
	LemaLoggingEvent toSend;

	private SocketThread create(boolean onDemand) {
		Factory<SocketFassade> provider = this.createFakeProvider();
		return new SocketThread("Test", 10L, provider, onDemand, 10, 10);
	}

	private Factory<SocketFassade> createFakeProvider() {
		Factory<SocketFassade> provider = new Factory<SocketFassade>() {

			public SocketFassade get() {
				return SocketThreadTest.this.socketFassade;
			}
		};
		return provider;
	}

	@Test(timeout = 1000)
	public void testConnectOkUndSendenEmpfangenBeacon() throws Exception {

		Mockito.when(this.socketFassade.read()).thenReturn(this.toSend);
		Mockito.when(this.socketFassade.write(Mockito.any())).thenReturn(true);
		this.toTest = this.create(false);

		// Run
		this.toTest.start();
		Thread.sleep(20);

		Object read = this.toTest.read();
		boolean write = this.toTest.write(this.toSend);

		this.toTest.cancel();
		this.toTest.join();

		// Verify
		Assert.assertEquals(this.toSend, read);
		Assert.assertEquals(true, write);
	}

	@Test(timeout = 1000)
	public void testConnectOkUndAutoDisconnectOnDemand() throws Exception {

		Mockito.when(this.socketFassade.write(Mockito.any())).thenReturn(true);
		this.toTest = this.create(true);

		// Run
		this.toTest.start();
		Thread.sleep(20);
		this.toTest.enqueForSending(this.toSend);
		Thread.sleep(30);

		// Verify
		Mockito.verify(this.socketFassade).write(this.toSend);
		Assert.assertEquals(null, this.toTest.getSocketFassade());

		// Cleanup
		this.toTest.cancel();
		this.toTest.join();
	}

	@Test(timeout = 1000)
	public void testConnectOkUndSendenFehler() throws Exception {

		Mockito.when(this.socketFassade.write(Mockito.any())).thenReturn(false);
		this.toTest = this.create(false);

		// Run
		this.toTest.start();
		Thread.sleep(20);

		boolean write = this.toTest.write(this.toSend);
		this.toTest.cancel();
		this.toTest.join();

		// Verify

		Assert.assertEquals(false, write);
	}

	public void testConnectOkUndEmpfangenFehler(boolean onDemand) throws Exception {

		Mockito.when(this.socketFassade.read()).thenThrow(new IOException());

		this.toTest = this.create(onDemand);

		// Run
		this.toTest.start();
		Thread.sleep(20);
		Object read = this.toTest.read();

		this.toTest.cancel();
		this.toTest.join();

		// Verify
		Assert.assertEquals(null, read);

	}

	@Test(timeout = 1000)
	public void testConnectOkUndEmpfangenFehlerBeacon() throws Exception {
		this.testConnectOkUndEmpfangenFehler(false);

	}

	@Test(timeout = 1000)
	public void testConnectOkUndEmpfangenFehlerOnDemand() throws Exception {
		this.testConnectOkUndEmpfangenFehler(true);
	}

	public void testConnectReconnect(boolean onDemand) throws Exception {
		// Setup
		Mockito.when(this.socketFassade.write(Mockito.any())).thenReturn(false);

		this.toTest = this.create(onDemand);

		// Run
		this.toTest.start();
		Thread.sleep(50);
		this.toTest.enqueForSending(this.toSend); // Jetzt bricht die Verbindung
													// ab
		Thread.sleep(150); // Hier muss sie wiederhergestellt sein.
		this.toTest.cancel();
		this.toTest.join();

		// Verify
		Assert.assertEquals(false, this.toTest.isRunning());
	}

	@Test(timeout = 1000)
	public void testConnectReconnectBecon() throws Exception {
		this.testConnectReconnect(false);
	}

	@Test(timeout = 1000)
	public void testConnectReconnectOnDemand() throws Exception {
		this.testConnectReconnect(false);
	}

	@Test(timeout = 1000)
	public void testDaemon() throws InterruptedException {
		this.toTest = this.create(false);
		Assert.assertEquals(true, this.toTest.isDaemon());

	}

}

/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.Assert;
import org.junit.Test;

import de.lema.appender.util.FixtureFactory;

public class ConnectionPropertiesTest {
	
	@Test(timeout = 2000)
	public void testResolveHost() throws UnknownHostException {
		ConnectionProperties toTest = new ConnectionProperties(
				FixtureFactory.createFakeIndentifikation(), "127.0.0.1", 4000);
 
		Assert.assertEquals(true, toTest.istValid());
		Assert.assertEquals(InetAddress.getByName(toTest.getHost()),
				toTest.getAddress());

	}
	

}

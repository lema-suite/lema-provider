/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ObjectReaderTest {
	private ObjectReader owRead;

	private ObjectWriter owWrite;

	private static File tempDir;

	@AfterClass
	public static void cleanUp() throws IOException {
		tempDir.delete();
	}

	@BeforeClass
	public static void init() throws IOException {
		tempDir = ObjectReaderWriterHelper.getTempDir();
	}

	@After
	public void tearDown() throws IOException {
		ObjectReaderWriterHelper.cleanup(owRead, tempDir);
		ObjectReaderWriterHelper.cleanup(owWrite, tempDir);
	}

	private void readUntilNull(List<Object> actual) throws IOException,
			ClassNotFoundException {
		for (Object read = owRead.read(); read != null; read = owRead.read()) {
			actual.add(read);
		}
	}

	@Test
	public void testWrite3Read3Cap3() throws IOException,
			ClassNotFoundException {
		// Setup
		File file = createFiles(3, 3);
		owWrite = null;
		owRead = new ObjectReader(file);
		List<Object> expected = createListMitDummies(3);
		List<Object> actual = new ArrayList<Object>();

		// Run
		readUntilNull(actual);

		// Verify
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testWrite3Read3Cap10() throws IOException,
			ClassNotFoundException {
		// Setup
		File file = createFiles(10, 3);
		owWrite = null;
		owRead = new ObjectReader(file);
		List<Object> expected = createListMitDummies(3);
		List<Object> actual = new ArrayList<Object>();

		// Run
		readUntilNull(actual);

		// Verify
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testWrite2Read2Cap3() throws IOException,
			ClassNotFoundException {
		// Setup
		File file = createFiles(3, 2);
		owWrite = null;
		owRead = new ObjectReader(file);
		List<Object> expected = createListMitDummies(2);
		List<Object> actual = new ArrayList<Object>();

		// Run
		readUntilNull(actual);

		// Verify
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testWrite100Read101Cap50() throws IOException,
			ClassNotFoundException {
		// Setup
		File file = createFiles(50, 3);
		owWrite = null;
		owRead = new ObjectReader(file);
		List<Object> expected = createListMitDummies(3);
		List<Object> actual = new ArrayList<Object>();

		// Run
		readUntilNull(actual);

		// Verify
		Assert.assertEquals(expected, actual);
	}

	private List<Object> createListMitDummies(int anzahl) {
		ArrayList<Object> back = new ArrayList<Object>();
		for (int i = 0; i < anzahl; i++) {
			back.add(ObjectReaderWriterHelper.createObjectDummy());
		}
		return back;
	}

	private File createFiles(int size, int objects) throws IOException {
		owWrite = ObjectReaderWriterHelper.createObjectWriter(size, tempDir);
		ObjectReaderWriterHelper.writeElemente(owWrite, objects);
		owWrite.close();
		File[] listFiles = tempDir.listFiles();
		Assert.assertEquals(1, listFiles.length);
		return listFiles[0];

	}

}

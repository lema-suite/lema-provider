/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.net.ServerSocket;
import java.util.concurrent.CountDownLatch;

import junit.framework.Assert;

import org.junit.Test;

import de.lema.appender.util.FixtureFactory;
import de.lema.appender.util.FixtureFactory.Helper;

public class SocketFassadeImplTest {

	@Test(timeout = 300)
	public void testOk() throws Exception {
		// Setup
		int port = 40000;
		final CountDownLatch serverReady = new CountDownLatch(1);
		final ServerSocket serversocket = new ServerSocket(port);
		final Helper helper = FixtureFactory.createHelperRunnable(serversocket,
				serverReady, false);
		final Thread thread = new Thread(helper);
		final ConnectionProperties data = FixtureFactory
				.createFakeConnectionData(port);

		// run
		thread.start();
		serverReady.await();
		SocketFassade toTest = SocketFassade.create(data);
		toTest.write(Integer.valueOf(1));
		toTest.read();

		// Verify
		Assert.assertEquals(false, toTest.istClosed());

		// Cleanup
		serversocket.close();
		helper.close();
		toTest.close();
		thread.join();

	}

	@Test(timeout = 300000)
	public void testCloseOnError() throws Exception {
		// Setup

		final CountDownLatch serverReady = new CountDownLatch(1);
		int port = 40010;
		final ServerSocket serversocket = new ServerSocket(port);
		final Helper helper = FixtureFactory.createHelperRunnable(serversocket,
				serverReady, false);
		final Thread thread = new Thread(helper);
		final ConnectionProperties data = FixtureFactory
				.createFakeConnectionData(port);

		// run
		thread.start();
		serverReady.await();
		SocketFassade toTest = SocketFassade.create(data);
		toTest.write(Integer.valueOf(1));
		// Server wird dicht gemacht
		serversocket.close();
		helper.close();
		thread.join();

		toTest.write(Integer.valueOf(1));

		// Verify
		Assert.assertEquals(true, toTest.istClosed());

	}

}

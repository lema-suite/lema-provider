/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.File;
import java.io.IOException;

public class ObjectReaderWriterHelper {
	static File getTempDir() throws IOException {
		File createTempFile = File.createTempFile("Dummy", "Dummy");
		File tempDir = new File(createTempFile.getParent() + File.separator
				+ "/ObjectWriterTest");
		tempDir.mkdirs();
		createTempFile.delete();
		return tempDir;
	}

	static void cleanup(AbstractObjectWriter ow, File tempDir)
			throws IOException {
		if (ow != null) {
			ow.close();
			ow = null;
		}
		File[] listFiles = tempDir.listFiles();
		for (File file : listFiles) {
			file.delete();
		}
	}

	static <T> ObjectWriter<T> createObjectWriter(int headersize, File tempDir) {
		return new ObjectWriter<T>(headersize, tempDir, "App-TestCase");
	}

	@SuppressWarnings("unchecked")
	static <T> void writeElemente(ObjectWriter<T> ow, int anzahl)
			throws IOException {
		for (int i = 0; i < anzahl; i++) {
			ow.write((T) createObjectDummy());
		}
	}

	static Object createObjectDummy() {
		return "DummyString";
	}

}

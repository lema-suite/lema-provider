/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.net;

import java.io.File;
import java.io.IOException;

import junit.framework.Assert;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class ObjectWriterTest {

	private static File tempDir;

	@AfterClass
	public static void cleanUp() throws IOException {
		tempDir.delete();
	}

	@BeforeClass
	public static void init() throws IOException {
		tempDir = ObjectReaderWriterHelper.getTempDir();
	}

	private ObjectWriter<Object> ow;

	@Test
	public void test4WritesSize3() throws IOException {
		// Setup
		ow = ObjectReaderWriterHelper.createObjectWriter(3, tempDir);

		// Run
		ObjectReaderWriterHelper.writeElemente(ow, 4);

		// Verify
		Assert.assertEquals(2, tempDir.list().length);
	}

	@Test
	public void testWrite1Size1() throws IOException {
		// Setup
		ow = ObjectReaderWriterHelper.createObjectWriter(1, tempDir);

		// Run
		ObjectReaderWriterHelper.writeElemente(ow, 1);

		// Verify
		Assert.assertEquals(1, tempDir.list().length);
	}

	@Test
	public void testWrite30Size1() throws IOException {
		// Setup
		ow = ObjectReaderWriterHelper.createObjectWriter(1, tempDir);

		// Run
		ObjectReaderWriterHelper.writeElemente(ow, 30);

		// Verify
		Assert.assertEquals(30, tempDir.list().length);
	}

	@Test
	public void testWrite30Size30() throws IOException {
		// Setup
		ow = ObjectReaderWriterHelper.createObjectWriter(30, tempDir);

		// Run
		ObjectReaderWriterHelper.writeElemente(ow, 30);

		// Verify
		Assert.assertEquals(1, tempDir.list().length);
	}

	@After
	public void tearDown() throws IOException {

		ObjectReaderWriterHelper.cleanup(ow, tempDir);
	}

}

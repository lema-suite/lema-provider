/*
 * #%L
 * lema-provider-socket
 * %%
 * Copyright (C) 2009 - 2021 LEMA
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
package de.lema.appender.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.CountDownLatch;

import de.lema.appender.Identifikation;
import de.lema.appender.net.ConnectionProperties;

public final class FixtureFactory {

	private FixtureFactory() {

	}

	public static final class Helper implements Runnable {
		private final boolean ignoreExecption;
		private final CountDownLatch latch;
		private final ServerSocket serversocket;
		private Socket socket;

		private Helper(boolean ignoreExecption, CountDownLatch latch, ServerSocket serversocket) {
			this.ignoreExecption = ignoreExecption;
			this.latch = latch;
			this.serversocket = serversocket;
		}

		public void run() {
			try {
				latch.countDown();
				socket = serversocket.accept();
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ois.readObject();
				oos.writeObject(Integer.valueOf(2));

			} catch (Exception e) {
				if (!ignoreExecption) {
					throw new RuntimeException(e);
				}
			}
		}

		public void close() throws IOException {
			socket.close();
		}
	}

	public static Thread createFakeServer(Helper runable) throws IOException {

		Thread thread = new Thread(runable);
		thread.setDaemon(true);
		return thread;
	}

	public static Helper createHelperRunnable(final ServerSocket serversocket, final CountDownLatch latch, final boolean ignoreExecption) {
		return new Helper(ignoreExecption, latch, serversocket);
	}

	public static Identifikation createFakeIndentifikation() {
		return Identifikation.create("dummy", 1, false, "1.0.0", "", true);
	}

	public static ConnectionProperties createFakeConnectionData(int port) {
		return new ConnectionProperties(createFakeIndentifikation(), "localhost", port);
	}
}
